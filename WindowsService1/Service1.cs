﻿using System;  
using System.Collections.Generic;  
using System.ComponentModel;  
using System.Data;  
using System.Diagnostics;  
using System.IO;
using System.Xml.Linq;  
using System.Linq;  
using System.ServiceProcess;  
using System.Text;  
using System.Threading.Tasks;  
using System.Timers;

namespace MESHFileService
{  

    public partial class Service1: ServiceBase {  

        Timer timer = new Timer(); // name space(using System.Timers;)  
        //public const string MESH_INCOMING_FOLDER = "C:\\DTSv5\\PathologyData\\in\\";
        public const string MESH_INCOMING_FOLDER = "C:\\IDC\\Projects\\SavingLives\\TestFiles\\";

        public Service1() {  

            InitializeComponent();  

        }  

        protected override void OnStart(string[] args) {  

            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);  
            timer.Interval = 30000; //number in milliseconds  
            timer.Enabled = true;  
        }  

        protected override void OnStop() {  

        }  

        private void OnElapsedTime(object source, ElapsedEventArgs e) {  

            HandleFiles();  
        }  

        public void HandleFiles() {

            
                DirectoryInfo d = new DirectoryInfo(MESH_INCOMING_FOLDER);
                string fromFolder;
                string errorFolder;
                string datafile;

                foreach (var incomingFile in d.GetFiles("*.ctl"))
                {
                                    
                    datafile = Path.GetFileNameWithoutExtension(incomingFile.Name) + ".dat";
                    string path = Path.Combine(MESH_INCOMING_FOLDER, datafile);

                    if (File.Exists(path))
                    {
                      string successStatus = "";
                      string fromDTS = "";
                      
                      //open XDocument and get values into variables
                      XDocument doc = XDocument.Load(path);
                      XElement e = doc.Root;

                      if (e != null)
                      {
                          if ((string)e.Element("From_DTS") != null)
                          {
                              fromDTS = e.Element("From_DTS").Value;
                          }

                          if ((string)e.Element("StatusRecord") != null)
                          {
                              var statusDetails = e.Element("StatusRecord").Elements();

                              if (statusDetails != null)
                              {
                                  foreach (XElement statDetail in statusDetails)
                                  {
                                      if ((string)statDetail.Element("Status") != null)
                                      {
                                          successStatus = statDetail.Element("Status").Value;
                                      }
                                  }
                              }
                          }

                      }

                      if (successStatus == "SUCCESS")
                      {
                          fromFolder = Path.Combine(MESH_INCOMING_FOLDER, fromDTS); ;
                          if (!Directory.Exists(fromFolder))
                          {
                              Directory.CreateDirectory(fromFolder);
                          } 
                          
                          File.Move(path,Path.Combine(fromFolder,datafile));
                          File.Delete(Path.Combine(MESH_INCOMING_FOLDER , incomingFile.Name)); 
                      }
                      else
                      {
                          errorFolder = Path.Combine(MESH_INCOMING_FOLDER, "Errors"); ;
                          if (!Directory.Exists(errorFolder))
                          {
                              Directory.CreateDirectory(errorFolder);
                          } 
                          
                          incomingFile.MoveTo(Path.Combine(errorFolder,incomingFile.Name));
                          File.Move(path,Path.Combine(errorFolder,datafile));
                      }
                            
                    }
                    else  // no matching .dat file in the folder
                    {
                          errorFolder = Path.Combine(MESH_INCOMING_FOLDER, "Errors"); ;
                          if (!Directory.Exists(errorFolder))
                          {
                              Directory.CreateDirectory(errorFolder);
                          } 
                          
                          incomingFile.MoveTo(Path.Combine(errorFolder, incomingFile.Name));
                    }
                 
                }
            }   

        }  
}
